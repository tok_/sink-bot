(ns sink.physics.b2
  (:require [sink.physics.types.body :as body])
  (:import (org.bytedeco.liquidfun b2Body b2BodyDef b2CircleShape b2Fixture b2ParticleSystem b2Vec2 b2World)
           (org.bytedeco.javacpp BytePointer Pointer)
           (java.nio.charset Charset)))

(def ^:private ^String charset-name "UTF-8")
(def ^:private ^Charset charset (Charset/forName charset-name))
(defn ^BytePointer string->pointer [s] (BytePointer. ^String s charset))
(defn ^String pointer->string [p] (.getString (BytePointer. ^Pointer p) charset))

(defn ^b2Vec2 vec2
  ([] (b2Vec2.))
  ([v] (b2Vec2. (first v) (second v)))
  ([x y] (b2Vec2. x y)))

(defn- b2-seq
  ([obj] (b2-seq obj nil))
  ([obj accu] (if (nil? obj) accu (recur (.GetNext obj) (conj accu obj)))))
(defn body-seq [^b2Body body] (b2-seq body))
(defn fixture-seq [^b2Fixture fix] (b2-seq fix))
(defn ps-seq [^b2ParticleSystem fix] (b2-seq fix))

(defn ^b2World ->world []
  (let [gravity (vec2 0.0 -10.0)]
    (b2World. gravity)))

(defn- ->vec [world particle-system shape]
  (let [body-def (b2BodyDef.)]
    (.type body-def (:dynamic body/body-type))
    (let [body (.CreateBody ^b2World world body-def)]
      (.CreateFixture body shape (.radius shape))
      (.DestroyParticlesInShape particle-system shape (.GetTransform body))
      body)))

(defn ->circle [world pos r]
  (let [shape (b2CircleShape.)
        particle-systems (ps-seq (.GetParticleSystemList world))]
    (.m_p shape (vec2 pos))
    (.m_radius shape r)
    (doseq [ps particle-systems] (->vec world ps shape))))
