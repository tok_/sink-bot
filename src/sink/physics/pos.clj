(ns sink.physics.pos
  (:require [sink.config :as config])
  (:import (org.bytedeco.liquidfun b2Vec2)))

(defn world-zero [] (b2Vec2. 0 0))
(defn local-zero [] (b2Vec2. (- sink.config/sim-extent-w) sink.config/sim-extent-h))

(defn- scale [n factor target sim target-ext] (-> n (* factor) (* target) (/ sim) (+ target-ext)))
(defn scale-x [x] (scale x 1.0 config/target-width config/sim-width config/target-extent-w))
(defn scale-y [y] (scale y -1.0 config/target-height config/sim-height config/target-extent-h))

(defn pos [^b2Vec2 pos] {:x (-> pos .x scale-x) :y (-> pos .y scale-y)})
(defn world->pixel [pos] {:x (-> pos :x scale-x) :y (-> pos :y scale-y)})

(defn- clamp-n [n lower upper] (-> n (max lower) (min upper)))
(defn- clamp-x [x] (clamp-n x 0 config/target-width))
(defn- clamp-y [y] (clamp-n y 0 config/target-height))
(defn clamp-pos [pos] {:x (-> pos :x clamp-x) :y (-> pos :y clamp-y)})
