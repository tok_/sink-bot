(ns sink.physics.world
  (:require [sink.config :as config]
            [sink.math :as math]
            [sink.physics.b2 :as b2]
            [sink.physics.types.body :as body]
            [sink.physics.types.particle :as particle]
            [clojure.tools.logging :as log])
  (:import (org.bytedeco.liquidfun
             b2BodyDef b2FixtureDef b2MassData b2ParticleGroupDef
             b2ParticleSystem b2ParticleSystemDef b2PolygonShape b2Vec2 b2World b2ParticleColor)))

(defn- ->sink-world []
  (let [world (b2/->world)
        ps-def (b2ParticleSystemDef.)
        r config/particle-radius]
    (.radius ps-def r)
    (.dampingStrength ps-def config/damping)
    (.density ps-def config/density)
    (.colorMixingStrength ps-def 0.02)
    ;(.repulsiveStrength ps-def 2.0)
    (let [ps (.CreateParticleSystem world ps-def)]
      world)))

(defn- ->box [body ps hx hy cx cy angle]
  (let [shape (b2PolygonShape.)
        fix-def (b2FixtureDef.)
        center (b2Vec2. cx cy)]
    (.SetAsBox shape hx hy center angle)
    (.shape fix-def shape)
    (.DestroyParticlesInShape ps shape (.GetTransform body))
    (.CreateFixture body fix-def)))

(defn- attach-pool [^b2World world ps]
  (let [body-def (b2BodyDef.)
        t 1.0]
    (.type body-def (:static body/body-type))
    (let [body (.CreateBody world body-def)]
      (->box body ps config/sim-extent-w t
             0 (* -1.0 (+ config/sim-extent-h t)) 0)
      (->box body ps t config/sim-height
             (* 1.0 (+ config/sim-extent-w t)) t 0)
      (->box body ps t config/sim-height
             (* -1.0 (+ config/sim-extent-w t)) t 0))))

(defn- set-colors [^b2ParticleSystem ps]
  (let [^b2ParticleColor color-buffer (.GetColorBuffer ps)
        wc config/water-color
        r (:r wc) g (:g wc) b (:b wc) a (:a wc)]
    (.Set color-buffer r g b a)))

(defn- attach-water [^b2ParticleSystem ps]
  (let [water-width config/sim-extent-w
        water-level config/sim-height
        shape (b2PolygonShape.)
        group-def (b2ParticleGroupDef.)
        x 0
        y (int (* 0.4 water-level))  ;(int (* water-level -0.5))
        ]
    (.SetAsBox shape water-width water-level)
    (.shape group-def shape)
    (.flags group-def (:water particle/particle-type))
    (.SetPosition group-def x (+ y 0.1))
    (let [group (.CreateParticleGroup ps group-def)]
      (log/debug "Particle group created: " (.GetBufferIndex group)))
    (set-colors ps)))

(defn- ->world []
  (let [^b2World world (->sink-world)
        ps (.GetParticleSystemList world)]
    (attach-pool world ps)
    (attach-water ps)
    (doseq [i (range config/warmup-steps)]
      (if (= (mod (inc i) 100) 0)
        (println "calculating warmup frame" (inc i) "of" config/warmup-steps))
      (.Step world config/ani-step config/velocity-iterations config/position-iterations))
    {:world world :ps ps}))

(def pics (atom (list)))
(def world-items (->world))

(defn attach-falling-square []
  (let [ps (:ps world-items)
        body-def (b2BodyDef.)]
    (.type body-def (:dynamic body/body-type))
    (let [body (.CreateBody (:world world-items) body-def)
          mass-data (b2MassData.)
          shape (b2PolygonShape.)
          fix-def (b2FixtureDef.)
          randomness 5.0
          initial-av (- (* (.nextFloat math/random) randomness) (* randomness 0.5))
          offset-angle (* math/tau -45 (/ 1 360))
          hx (* 5 config/particle-radius) hy (* 5 config/particle-radius)]
      (.SetAngularVelocity body initial-av)                 ;fixme
      (.SetFixedRotation body false)

      (.mass mass-data 1.0)
      (.position mass-data 0)
      (.center mass-data (b2Vec2. 0.0 0.0))
      (.SetMassData body mass-data)

      (.SetAsBox shape hx hy (b2Vec2. 0 config/sim-extent-h) offset-angle)
      (.shape fix-def shape)

      (.density fix-def config/fixture-density)
      (.friction fix-def config/friction)
      (.restitution fix-def config/restitution)

      (.DestroyParticlesInShape ps shape (.GetTransform body))
      (.CreateFixture body fix-def)
      body)))
