(ns sink.physics.types.particle)

;https://google.github.io/liquidfun/API-Ref/html/b2_particle_8h.html
(def ^:const particle-type
  {:water                     0
   :zombie                    (bit-shift-left 1 1)
   :wall                      (bit-shift-left 1 2)
   :spring                    (bit-shift-left 1 3)
   :elastic                   (bit-shift-left 1 4)
   :viscous                   (bit-shift-left 1 5)
   :powder                    (bit-shift-left 1 6)
   :tensile                   (bit-shift-left 1 7)
   :color-mixing              (bit-shift-left 1 8)
   :destruction-listener      (bit-shift-left 1 9)
   :barrier                   (bit-shift-left 1 10)
   :static-pressure           (bit-shift-left 1 11)
   :reactive                  (bit-shift-left 1 12)
   :repulsive                 (bit-shift-left 1 13)
   :fixture-contact-listener  (bit-shift-left 1 14)
   :particle-contact-listener (bit-shift-left 1 15)
   :fixture-contact-filter    (bit-shift-left 1 16)
   :particle-contact-filter   (bit-shift-left 1 17)})
