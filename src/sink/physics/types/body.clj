(ns sink.physics.types.body)

(def ^:const body-type {:static 0 :kinematic 1 :dynamic 2})
