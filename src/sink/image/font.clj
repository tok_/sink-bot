(ns sink.image.font
  (:require [sink.config :as config])
  (:import (java.awt Font)))

(def ^:private names
  {:courier "Courier"
   :dialog "Dialog"
   :helvetica "Helvetica"
   :times "TimesRoman"
   :zapf "ZapfDingbats"})

(defn- normal [key] (Font. (get names key) Font/BOLD config/font-size))
(defn- back [key] (Font. (get names key) Font/BOLD config/font-back-size))

(defn ->font
  ([] (->font (first names)))
  ([key] (->font key :normal))
  ([key type] (case type :back (back key) (normal key))))
