(ns sink.image.operation.rescale
  (:import (java.awt.image RescaleOp)
           (java.awt RenderingHints)))

(def tint-blue
  (let [scale-factors (float-array [1 1 1.2])
        offsets (float-array [0 0 64])
        hints (RenderingHints. {})]
    (RescaleOp. scale-factors offsets hints)))
