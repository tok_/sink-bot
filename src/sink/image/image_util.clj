(ns sink.image.image-util
  (:require [sink.config :as config])
  (:import (java.awt RenderingHints)
           (java.awt.image BufferedImage RenderedImage)
           (java.net URL)
           (java.io ByteArrayOutputStream)
           (javax.imageio ImageIO)))

(defn ^RenderedImage ->image
  ([] (->image config/target-width config/target-height))
  ([_] (->image))
  ([w h] (BufferedImage. w h BufferedImage/TYPE_INT_ARGB)))

(defn image-from-template [source]
  (let [w (.getWidth source)
        h (.getHeight source)
        type (.getType source)]
    (BufferedImage. w h type)))

(defn perform-op [source op]
  (let [target (image-from-template source)]
    (.filter op source target)
    target))

(defn ^RenderedImage shrink-to [w] config/shrink-to)

(defn ^RenderedImage scale [image factor]
   (let [w (.getWidth image)
         h (.getHeight image)
         type (.getType image)
         scaled-w (* factor w)
         scaled-h (* factor h)
         scaled (BufferedImage. scaled-w scaled-h type)
         ctx (.createGraphics scaled)
         render-key RenderingHints/KEY_INTERPOLATION
         render-value RenderingHints/VALUE_INTERPOLATION_BILINEAR]
     (.setRenderingHint ctx render-key render-value)
     (.drawImage ctx image 0 0 scaled-w scaled-h 0 0 w h nil)
     (.dispose ctx)
     scaled))

(defn ^BufferedImage url->image [photo-url] (-> photo-url URL. ImageIO/read))

(defn image->bytes [^RenderedImage image]
  (let [os (ByteArrayOutputStream.)]
    (ImageIO/write image config/output-format os)
    (.toByteArray os)))
