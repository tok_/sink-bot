(ns sink.image.draw
  (:require [sink.bot.client :as client]
            [sink.config :as config]
            [sink.image.font :as font]
            [sink.math :as math]
            [sink.physics.pos :as pos]
            [sink.physics.world :as world])
  (:import (java.awt BasicStroke Color Font Graphics2D Polygon RenderingHints)
           (java.awt.geom AffineTransform)
           (java.awt.image AffineTransformOp BufferedImage BufferedImageOp)
           (org.bytedeco.liquidfun b2Body b2Fixture b2ParticleSystem b2Vec2 b2World)))

(def drops-cache (atom {}))
(defn- water-drop [radius particle-color]
  (let [r (int (.r particle-color))
        g (int (.g particle-color))
        b (int (.b particle-color))
        a (int (.a particle-color))
        key (str radius r g b a)
        image (BufferedImage. radius radius BufferedImage/TYPE_INT_ARGB)
        g2d (.createGraphics image)]
    (if (contains? @drops-cache key)
      (get @drops-cache key)
      (do
        (.setRenderingHint g2d RenderingHints/KEY_ANTIALIASING RenderingHints/VALUE_ANTIALIAS_OFF)
        (.setRenderingHint g2d RenderingHints/KEY_RENDERING RenderingHints/VALUE_RENDER_QUALITY)
        (.setRenderingHint g2d RenderingHints/KEY_STROKE_CONTROL RenderingHints/VALUE_STROKE_PURE)
        (.setRenderingHint g2d RenderingHints/KEY_TEXT_ANTIALIASING RenderingHints/VALUE_TEXT_ANTIALIAS_OFF)
        (.setColor g2d (Color. r g b a))
        (.fillArc g2d 0 0 radius radius 0 360)
        (swap! drops-cache assoc key image)
        image))))

(defn- write [^Graphics2D g2d ^String text ^Font font ^Color color offset]
  (.setFont g2d font)
  (.setColor g2d color)
  (.drawString g2d text ^int offset ^int offset))

(defn draw-text [^BufferedImage image ^String text ^Color color]
  (let [^Graphics2D g2d (.createGraphics image)
        ^Font font (font/->font :courier :normal)
        offset 20]
    (write g2d text font color offset)))

(defn- col->awt [c] (Color. ^int (:r c) ^int (:g c) ^int (:b c) ^int (:a c)))
(defn- draw-background [^Graphics2D g2d]
  (.setColor g2d (col->awt config/background-color))
  (.fillRect g2d 0 0 config/target-width config/target-height))

(defn- draw-poly [^Graphics2D g2d ^Polygon poly]
  (.setColor g2d Color/red)
  (.setStroke g2d (BasicStroke. 4))
  (.drawPolygon g2d poly))

(defn- draw-image [^Graphics2D g2d ^Polygon poly ^b2Body body ^BufferedImage pic]
  (let [w (.getWidth pic) h (.getHeight pic)
        x-scale (float 1.0)
        y-scale (float 1.0)
        pic-w2 (* w 0.5 x-scale) pic-h2 (* h 0.5 y-scale)
        theta (+ (* math/tau 45 (/ 1 360)) (* -1.0 (.GetAngle body)))
        x-shear (float 0.0)
        y-shear (float 0.0)
        x-translate (float (- pic-w2))
        y-translate (float (- pic-h2))
        at (AffineTransform. x-scale y-shear x-shear y-scale x-translate y-translate)]
    (.rotate at theta pic-w2 pic-h2)
    (let [x (math/average (.xpoints poly))
          y (math/average (.ypoints poly))
          ^BufferedImageOp op (AffineTransformOp. at AffineTransformOp/TYPE_BILINEAR)]
      (.drawImage g2d pic op ^int x ^int y))))

(defn- fixtures->points
  ([^b2Fixture fixture] (flatten (fixtures->points fixture nil 0)))
  ([^b2Fixture fixture accu i]
   (if (some? fixture)
     (let [^b2Body body (.GetBody fixture)
           transform (.GetTransform body)
           pos (.GetWorldCenter body)
           x (.x pos)
           y (.y pos)
           rot-sin (.GetRotationSin transform)
           rot-cos (.GetRotationCos transform)
           top-left {:x (- x rot-sin) :y (+ y rot-cos)}
           top-right {:x (+ x rot-cos) :y (+ y rot-sin)}
           bottom-right {:x (+ x rot-sin) :y (- y rot-cos)}
           bottom-left {:x (- x rot-cos) :y (- y rot-sin)}
           pts [bottom-right top-right top-left bottom-left] ;CCW
           points (map (fn [v] {:x (pos/scale-x (:x v)) :y (pos/scale-y (:y v))}) pts)]
       (recur (.GetNext fixture) (conj accu points) (inc i)))
     accu)))

(defn- points->polygon [points]
  (if (not (nil? points))
    (let [x-points (int-array (map #(:x %) points))
          y-points (int-array (map #(:y %) points))]
      (Polygon. x-points y-points (count points)))))

(defn- fixtures->Polygons [^b2Fixture fixtures ^b2Body body accu]
  (if (some? fixtures)
    (let [points (fixtures->points fixtures)
          polygon (points->polygon points)]
      (fixtures->Polygons (.GetNext fixtures) body (conj accu polygon)))
    accu))

(def ^AffineTransform at (AffineTransform. 1.0 0.0 0.0 1.0 0.0 0.0))
(def ^AffineTransformOp op (AffineTransformOp. at AffineTransformOp/TYPE_BILINEAR))

(defn- draw-particle-systems [^Graphics2D g2d ^b2ParticleSystem ps]
  (when (some? ps)
    (doseq [i (range (.GetParticleCount ps))]
      (let [world-pos (b2Vec2. (.GetParticlePositionX ps i) (.GetParticlePositionY ps i))
            pos (pos/pos world-pos)
            x (min config/target-width (max 0 (:x pos)))
            y (min config/target-height (max 0 (:y pos)))
            radius (-> ps .GetRadius (* 100) int)
            particle-color (.GetColorBuffer ps)
            ^BufferedImage drop-image (water-drop radius particle-color)
            xx (int (- x (* 0.5 radius)))
            yy (int (- y (* 0.5 radius)))]
        (.drawImage g2d drop-image op xx yy)))))

(defn- draw-body [^Graphics2D g2d ^b2Body body ^BufferedImage i pic-index]
  (let [fixtures (.GetFixtureList body)
        polygons (fixtures->Polygons fixtures body nil)]
    (doseq [poly polygons]
      (if (= (count polygons) 1)
        (do
          ;(draw-poly g2d poly)
          (draw-image g2d poly body (nth @world/pics pic-index)))))))

(defn- draw-bodies [g2d body i pic-index]
  (when (some? body)
    (draw-body g2d body i pic-index)
    (draw-bodies g2d (.GetNext body) i (inc pic-index))))

(defn- draw-frame [message-id ^b2ParticleSystem ps ^BufferedImage image world-items i]
  (let [^Graphics2D g2d (.createGraphics image)
        ^b2World world (:world world-items)
        ^b2Body falling-square (:falling-square world-items)
        report-steps 10]
    (if (= (mod (inc i) report-steps) 0)
      (let [msg (apply str (repeat (/ (inc i) 10) "."))]
        (client/update-message message-id msg)))
    (.setRenderingHint g2d RenderingHints/KEY_ANTIALIASING RenderingHints/VALUE_ANTIALIAS_OFF)
    (.setRenderingHint g2d RenderingHints/KEY_RENDERING RenderingHints/VALUE_RENDER_QUALITY)
    (.setRenderingHint g2d RenderingHints/KEY_STROKE_CONTROL RenderingHints/VALUE_STROKE_PURE)
    (.setRenderingHint g2d RenderingHints/KEY_TEXT_ANTIALIASING RenderingHints/VALUE_TEXT_ANTIALIAS_OFF)
    (draw-background g2d)
    (draw-particle-systems g2d ps)
    (draw-bodies g2d falling-square i 0)
    (draw-text image (str i) Color/black)
    (.Step world config/ani-step config/velocity-iterations config/position-iterations)
    image))

(defn draw-all [image-frames world-items message-id]
  (let [world (:world world-items)
        ps (.GetParticleSystemList world)
        n (count image-frames)]
    {:frames         (map #(draw-frame message-id ps (nth image-frames %) world-items %) (range n))
     :particle-count (.GetParticleCount ps)}))
