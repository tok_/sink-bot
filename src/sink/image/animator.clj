(ns sink.image.animator
  (:require [sink.config :as config])
  (:import (java.awt.image BufferedImage)
           (java.io ByteArrayOutputStream File)
           (javax.imageio ImageIO ImageTypeSpecifier IIOImage ImageWriter)
           (javax.imageio.metadata IIOMetadataNode IIOMetadata)
           (org.w3c.dom Node)
           (org.jcodec.common.io NIOUtils ByteBufferSeekableByteChannel)
           (org.jcodec.api SequenceEncoder)
           (org.jcodec.common.model Rational ColorSpace)
           (org.jcodec.scale AWTUtil)
           (java.nio.file Files)
           (java.nio.channels Channels WritableByteChannel)
           (java.nio ByteBuffer)
           (org.jcodec.common Codec Format)))

(defn- gce? [^Node node] (-> node .getNodeName (= "GraphicControlExtension")))
(defn- find-gce [child] (if (gce? child) child (-> child .getNextSibling recur)))
(defn- find-gc-extension [root] (-> root .getFirstChild find-gce))
(defn- app-extensions []
  (let [aes (IIOMetadataNode. "ApplicationExtensions")
        ae (IIOMetadataNode. "ApplicationExtension")
        uo (byte-array [(byte 0x1) (byte 0x0) (byte 0x0)])]
    (.setAttribute ae "applicationID" "NETSCAPE")
    (.setAttribute ae "authenticationCode" "2.0")
    (.setUserObject ae uo)
    (.appendChild aes ae)
    aes))

(defn- not-gif? [mf] (not= "javax_imageio_gif_image_1.0" mf))
(defn- msg [mf] (str "Metadata format unknown: " mf))
(defn- iae [mf] (IllegalArgumentException. ^String (msg mf)))
(defn- check-mf [mf] (if (not-gif? mf) (-> mf iae throw)))
(defn- first-frame? [image-index] (= image-index 0))
(defn- configure [^IIOMetadata meta-data delay image-index]
  (let [metadata-format (.getNativeMetadataFormatName meta-data)]
    (check-mf metadata-format)
    (let [root (.getAsTree meta-data metadata-format)
          gce (find-gc-extension root)]
      (.setAttribute gce "userDelay" "FALSE")
      (.setAttribute gce "delayTime" delay)
      (if (first-frame? image-index) (.appendChild root (app-extensions)))
      (.setFromTree meta-data metadata-format root))))

(defn- write-frame [^BufferedImage src ^ImageWriter iw delay image-index]
  (let [iwp (.getDefaultWriteParam iw)
        type-spec (ImageTypeSpecifier. src)
        metadata (.getDefaultImageMetadata iw type-spec iwp)]
    (configure metadata delay image-index)
    (let [ii (IIOImage. src nil metadata)]
      (.writeToSequence iw ii nil))))

(defn- save-animate [frames delay]
  (let [format config/output-format
        ^ImageWriter iw (-> format ImageIO/getImageWritersByFormatName .next)
        os (ByteArrayOutputStream.)
        ios (ImageIO/createImageOutputStream os)]
    (.setOutput iw ios)
    (.prepareWriteSequence iw nil)
    (doseq [index (range (count frames))]
      (write-frame (nth frames index) iw delay index))
    (.endWriteSequence iw)
    (.close ios)
    (.toByteArray os)))

(defn- animate-gif [frames]
  "Creates an animated GIF from the frames."
  (save-animate frames config/ani-delay))

(defn- animate-h264 [frames]
  "Creates a soundless H.264/MPEG-4 AVC video from the frames."
  (let [output (File. "sink.mp4")
        channel (NIOUtils/writableChannel output)
        fps (Rational. config/frames-per-second-h264 1)
        encoder (SequenceEncoder/createWithFps channel fps)]
    (doseq [^BufferedImage frame frames]
      (.encodeNativeFrame
        encoder
        (AWTUtil/fromBufferedImage frame ColorSpace/RGB)))
    (.finish encoder)
    (NIOUtils/closeQuietly channel)
    (Files/readAllBytes (.toPath (.getAbsoluteFile output)))))

(defn animate [frames]
  (if (= config/mode :GIF)
    (animate-gif frames)
    (animate-h264 frames)))
