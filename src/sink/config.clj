(ns sink.config
  (:require [sink.math :as math]
            [clojure.java.io :as io]))

(def ^:private ^:const bot-name "@disco_sink_bot")
(def ^:private ^:const sink-command "sink")

(def ^:const secret-token (-> "secret-token.txt" io/resource slurp))

;; To find out the id of a group, invite @get_id_bot and do
;; /my_id@get_id_bot. Use the last number labeled "Your group Chat ID".
(def ^:const chat-id -400766734)                            ;@SINK
;(def chat-id -1001281414266)                                ;@discosoc

(def ^:const target-width 610)                              ;pixels
(def ^:const target-height target-width)                    ;pixels
(def ^:const target-extent-w (int (* target-width 0.5)))
(def ^:const target-extent-h (int (* target-height 0.5)))

(def ^:const output-formats {:GIF :H264})
(def ^:const mode :H264)

(def ^:const output-format (if (= mode :GIF) "gif" "mp4"))
(def ^:const file-name (str "sink." output-format))

(def ^:const ^float shrink-to 233.0)

(def ^:const font-size 20)
(def ^:const font-border-size 2)
(def ^:const font-back-size (+ font-size font-border-size font-border-size))

(def ^:const frames-per-second-gif 10)
(def ^:const frames-per-second-h264 30)
(def ^:const fps (if (= mode :GIF) frames-per-second-gif frames-per-second-h264))

(def ^:const duration-seconds-gif 7)
(def ^:const duration-seconds-h264 5)
(def ^:const max-particle-count 300000)
(def ^:const total-frames-gif (* frames-per-second-gif duration-seconds-gif))
(def ^:const total-frames-h264 (* frames-per-second-h264 duration-seconds-h264))
(def ^:const total-frames (if (= mode :GIF) total-frames-gif total-frames-h264))
(def ^:const ani-step-gif (/ 1.0 frames-per-second-gif))
(def ^:const ani-step-h264 (/ 1.0 frames-per-second-h264))
(def ^:const ani-step (if (= mode :GIF) ani-step-gif ani-step-h264))
(def ^:const sim-width 10.0)                                ;meters
(def ^:const sim-height (/ (* sim-width target-height) target-width))
(def ^:const sim-extent-w (* sim-width 0.5))
(def ^:const sim-extent-h (* sim-height 0.5))
(def ^:const ani-delay (str (int ani-step)))
(def ^:const velocity-iterations 6)                         ;default 6
(def ^:const position-iterations 2)                         ;default 2
(def ^:const warmup? true)
(def ^:const warmup-steps (if warmup? (* fps 100) 0))

;; particle physics
(def ^:const particle-radius 0.2)
(def ^:const density 1.0)
(def ^:const damping 0.5)                                   ;higher value -> "boil"
;; fixture physics
(def ^:const friction 0.8)
(def ^:const restitution 1.0)
(def ^:const fixture-density 3.0)

;; colors
(def ^:const water-color {:r 127 :g 127 :b 255 :a 127})
(def ^:const background-color {:r 223 :g 223 :b 223 :a 255})
