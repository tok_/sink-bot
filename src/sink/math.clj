(ns sink.math
  (:import (java.util Random)))

(def ^:const phi 1.618033988749895)
(def ^:const tau (* Math/PI 2))

(def random (Random.))

(defn average [coll] (/ (reduce + coll) (count coll)))
