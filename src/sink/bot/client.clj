(ns sink.bot.client
  (:require [clojure.tools.logging :as log]
            [sink.config :as config]
            [sink.image.image-util :as image-util])
  (:import (com.pengrad.telegrambot.request GetFile SendAnimation SendMessage EditMessageText EditMessageMedia DeleteMessage)
           (com.pengrad.telegrambot TelegramBot)
           (com.pengrad.telegrambot.response SendResponse)
           (com.pengrad.telegrambot.model Message)))

(def bot (TelegramBot. config/secret-token))

(defn- id->url [photo-id]
  (let [photo-req (GetFile. photo-id)
        file-response (.execute bot photo-req)
        file-info (.file file-response)]
    (.getFullFilePath bot file-info)))

(defn photo->url [photo] (id->url (.fileId photo)))

(defn send-empty-media []
  (let [image (image-util/->image 1 1)
        bytes (image-util/image->bytes image)
        message (SendAnimation. config/chat-id ^bytes bytes)]
    (.caption message ".")
    (.fileName message config/file-name)
    (let [^SendResponse response (.execute bot message)]
      (println response)
      (-> response .message .messageId))))

(defn send-media [bytes caption]
  (let [message (SendAnimation. config/chat-id ^bytes bytes)]
      (.caption message caption)
      (let [^SendResponse response (.execute bot message)]
        (log/info response))))

(defn send-message [text]
  (let [message (SendMessage. config/chat-id text)
        ^SendResponse response (.execute bot message)]
      (-> response .message .messageId)))

(defn update-message [message-id text]
  (let [edit (EditMessageText. config/chat-id message-id text)]
    (.execute bot edit)))

(defn remove-message [message-id]
  (.execute bot (DeleteMessage. config/chat-id message-id)))
