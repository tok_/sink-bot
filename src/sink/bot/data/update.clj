(ns sink.bot.data.update
  (:require [clojure.tools.logging :as log]))

(defn- data-map [chat user message]
  {:user     (.from message)
   :chat     (.chat message)
   :username (.username user)
   :title    (.title chat)
   :text     (.text message)
   :photos   (.photo message)})

(defn log [data] (log/info (:title data) (:username data) (:text data)))

(defn update->data [update]
  (let [message (.message update)
        chat (.chat message)
        user (.from message)]
    (data-map chat user message)))
