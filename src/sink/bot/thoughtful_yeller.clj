(ns sink.bot.thoughtful-yeller
  (:require [sink.text.md :as md]))

(defn- weighted-random [pairs]
  (let [weights (vals pairs)
        cutoffs (reductions + weights)
        weighted (zipmap cutoffs (keys pairs))
        total (reduce + weights)
        selection (rand-int total)]
    (val (first (filter #(< selection (key %)) weighted)))))

(defn- positive []
  {"Yay!"                                             800
   "Hah."                                             200
   "LOL."                                             500
   "LMAO."                                            400
   "Nice."                                            200
   "Great."                                           100
   "Bruh."                                            100
   "Finally!"                                         100
   (str "YES... HA HA HA... " (md/bold "YES") ".")    100
   "kek."                                             50
   "lel."                                             50
   "\"lol\", \"lmao\""                                50})

(defn- fails [subject object]
  {(str subject "tried to sink" object ". But it failed.") 100
   (str "WTF" subject "you cannot sink" object "now.")     100
   "But now we shall both surely drawn, said the RNG."     100
   "SURPRISE MFER. YOU SINK!"                              10})

(defn- actions []
  {"sank"       1000
   "has sunken" 800
   "has sinked" 100
   "did sink"   700
   "sinked"     50
   "sink'd"     20
   "senk"       10
   "sunk"       50
   "sonk"       5})

(defn yell [sinker object]
  (if (<= (rand) 0.05)
    (-> (weighted-random (fails sinker object)) shuffle first)
    (let [action (weighted-random (actions))
          exclaim (weighted-random (positive))
          exclaim-again (if (<= (rand) 0.5) (weighted-random (positive)) "")]
      (clojure.string/join " " (list exclaim sinker action (str object ".") exclaim-again)))))
