(ns sink.bot.listener
  (:require [clojure.tools.logging :as log]
            [sink.bot.data.update :as update]
            [sink.config :as config]
            [sink.image.animator :as animator]
            [sink.image.draw :as draw]
            [sink.image.image-util :as image-util]
            [sink.physics.world :as world]
            [sink.bot.thoughtful-yeller :as yeller]
            [sink.bot.client :as client])
  (:import (com.pengrad.telegrambot UpdatesListener)))

(defn- ->frames [] (->> config/total-frames range (map #(image-util/->image %))))

(defn- process-photo [message-id photo]
  (when (some? photo)
    (let [photo-url (client/photo->url photo)
          original (image-util/url->image photo-url)
          scaled (image-util/scale original 1.15)]
    (swap! world/pics conj scaled)
    (let [image-frames (->frames)
          world-items (assoc world/world-items :falling-square (world/attach-falling-square))
          result (draw/draw-all image-frames world-items message-id)
          frames (:frames result)
          particle-count (:particle-count result)
          caption (yeller/yell "foo" "bar")
          animation (animator/animate frames)]
      (log/debug photo-url)
      (client/remove-message message-id)
      (client/send-media animation caption)))) )

(defn- process-photos [message-id photos]
  (process-photo message-id (first photos)))

(defn- proc [updates]
  (doseq [update updates]
    (let [data (update/update->data update)
          message-id (client/send-message ".")]
      (update/log data)
      (process-photos message-id (:photos data))))
  UpdatesListener/CONFIRMED_UPDATES_ALL)

(defn ->listener []
  (reify UpdatesListener (process [_ updates] (proc updates))))
