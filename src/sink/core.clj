(ns sink.core
  (:require [sink.bot.listener :as listener]
            [sink.config :as config]
            [clojure.tools.logging :as log]
            [sink.bot.client :as client])
  (:import (com.pengrad.telegrambot TelegramBot)))

(defn -main [& _]
  (let [listener (listener/->listener)]
    (.setUpdatesListener client/bot listener)
    (log/info "Connected..")))
