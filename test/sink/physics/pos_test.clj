(ns sink.physics.pos-test
  (:require [sink.physics.pos :refer :all]
            [clojure.test :refer :all]))

(defn- do-test [expected actual]
  (testing (is (= expected actual))))

(deftest position-center-x-test
  (let [expected sink.config/target-extent-w
        actual (-> (world-zero) pos :x)]
    (do-test expected actual)))

(deftest position-center-y-test
  (let [expected sink.config/target-extent-h
        actual (-> (world-zero) pos :y)]
    (do-test expected actual)))

(deftest position-top-left-x-test
  (let [expected 0
        actual (-> (local-zero) pos :x)]
    (do-test expected actual)))

(deftest position-top-left-y-test
  (let [expected 0
        actual (-> (local-zero) pos :y)]
    (do-test expected actual)))
