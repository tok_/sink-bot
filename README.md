# SINK Bot #

A bot for playing SINK on Telegram.

### SINK ###

A game by Ala Hera, E.L., N.S.; RAYVILLE APPLE PANTHERS

* __PLAYERS:__ 
  SINK is played by Discordians and people of much ilk.

* __PURPOSE:__
  To sink object or an object or a thing... in water or mud or anything you can sink something in.

* __RULES:__
  Sinking is allowed in any manner.  To date, ten pound chunks of mud were used to sink a tobacco can.  It is preferable to have a pit of water or a hole to drop things in.  But rivers - bays - gulfs - I dare say even oceans can be used.

* __TURNS are taken thusly:__
  Who somever gets the junk up and in the air first.

* __DUTY:__
  It shall be the duty of all persons playing "SINK" to help find more objects to sink, once one object is sunk.

* __UPON SINKING:__
  The sinked shall yell "I sank it!" or something equally as thoughtful.

* __NAMING OF OBJECTS:__
  is some times desirable.  The object is named by the finder of such object and whoever sinks it can say for instance, "I sunk Columbus, Ohio!"


## Did you know?
According to "The Birth of Aphrodite" as described in Principia Harmonia, the Game of Sink was invented by the Greek God Cronus after he castrated his father Uranus.
