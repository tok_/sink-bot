(defproject sink-bot "0.7.7-SNAPSHOT"
  :description "SINK Telegram Bot"
  :url "https://bitbucket.org/tok_/sink-bot"
  :dependencies [[com.github.pengrad/java-telegram-bot-api "6.2.0"]
                 [org.bytedeco/liquidfun "master-1.5.7"]
                 [org.bytedeco/liquidfun-platform "master-1.5.7"]
                 [org.clojure/clojure "1.11.1"]
                 [org.clojure/tools.logging "1.2.4"]
                 [org.jcodec/jcodec "0.2.5"]
                 [org.jcodec/jcodec-javase "0.2.5"]]
  :aot [sink.core]
  :main sink.core)
